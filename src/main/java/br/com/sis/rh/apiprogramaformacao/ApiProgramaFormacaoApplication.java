package br.com.sis.rh.apiprogramaformacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiProgramaFormacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiProgramaFormacaoApplication.class, args);
	}

}
